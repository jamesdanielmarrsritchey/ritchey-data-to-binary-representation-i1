<?php
$location = realpath(dirname(__FILE__));
require_once $location . '/data_to_binary_representation_v1.php';
$return = data_to_binary_representation_v1('This is example data.', TRUE);
if ($return === FALSE){
	echo "FALSE\n";
} else {
	echo "{$return}\n";
}
?>